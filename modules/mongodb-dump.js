'use strict'

const MongoClient = require('mongodb').MongoClient
const fs = require('fs-extra')

/**
 * @name getAllCollections
 * @function
 * @param { Object } db MongoDB DB instance
 * @returns { Promise } returns an array of collections
 */
const getAllCollections = (db) => {
  return new Promise((resolve, reject) => {
    /* Get all collections */
    return db
      .listCollections()
      .toArray()
      .then((data) => resolve(data.map((ele) => ele.name)))
      .catch((err) => reject(err))
  })
}

/**
 * @function
 * @param { Array } collections Array of database collection names
 * @param { Object } db Mongodb DB instance
 * @returns { void }
 */
const dumpCollections = async (collections, db) => {
  /* For each collection, save it as collection_name.json */
  const saveDocumentTofile = async (collection) => {
    const documents = await db.collection(collection).find({}).toArray()

    return fs.writeFile(
      `./backup/dump/${collection}.json`,
      JSON.stringify(documents, null, 2),
      (err) => {
        if (err) console.log(err)
      }
    )
  }

  console.log(`Saving ${collections.length} collections...`)
  for (const item of collections) {
    await saveDocumentTofile(item)
  }

  return console.log('📝  Collections were saved!')
}

/**
 * @function
 * @description If it does not find the path, it will create it.
 * @param { string } path Path to the folder to be created
 * @returns { Promise }
 */
const makeDir = (path) => {
  if (fs.existsSync(path)) return
  return fs.mkdir(path, { recursive: true }, (err) => {
    if (err) console.log(err)
    console.log('Folder has been created!')
  })
}

/**
 * @function
 * @description Removes the folder passed as string.
 * @param { string } path Path to the folder to be removed
 * @returns { Promise} returns status on success
 */
const removeDir = async (path) => {
  if (fs.existsSync(path)) {
    fs.removeSync(path)
    return {
      status: 'success',
      msg: `${path} was successfuly removed `
    }
  }
}

/**
 * @function
 * @param { string } uri  MongoDB URI
 * @param { string } dbName MongoDB database name
 * @param { string } path  Backup folder path (it will create one if it does not exist)
 */
const mongoDump = async (uri, dbName, path) => {
  // Use connect method to connect to the Server
  const client = new MongoClient(uri + dbName, { useUnifiedTopology: true })
  /* Create the backup folder at given path */
  await makeDir(path)

  /* Return a new promise with a success status if the collections was saved and connection to db was closed. */
  return new Promise((resolve, reject) => {
    try {
      client.connect(async function () {
        /* Init the database instance */
        const db = client.db()
        /* Get all collections in a database */
        const collections = await getAllCollections(db)

        /* Return a success status if the dump was succesfull */
        return await dumpCollections(collections, db).then(() => {
          client.close().then(() =>
            resolve({
              status: 'success',
              msg: 'Succesfully dumped your collections'
            })
          )
        })
      })
    } catch (err) {
      console.log('catch: ', err)
      reject(err)
    }
  })
}

module.exports = {
  mongoDump,
  removeDir
}
