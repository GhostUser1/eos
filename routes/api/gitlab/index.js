const express = require('express')
const router = express.Router()
const fetch = require('node-fetch')
const axios = require('axios')
/* eslint-disable */
router.get('/', (req, res, next) => {
  ;(async () => {
    try {
      const gitlabReq = await fetch(
        'https://gitlab.com/api/v4/projects/3276752/repository/tags'
      )
      const gitRes = await gitlabReq.json()
      res.status(200).send(gitRes)
    } catch (err) {
      console.log(err)
    }
  })()
})

router.get('/repository/', async (req, res, next) => {
  const repositoryUrl = `https://gitlab.com/api/v4/projects/14105386/repository/files/README.md/raw?ref=master`

  const request = await axios.get(repositoryUrl)
  const response = await request.data

  res.status(200).send(response)
})

module.exports = router
