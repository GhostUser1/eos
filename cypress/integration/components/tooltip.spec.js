describe('Tooltip component', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-layout.html')
    cy.get('.js-user-profile').click()
  })

  // As part of best practices, when selecting DOM elements try to target data-attr and avoid changing classes or ids : https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements
  it('should truncate the text bases on data-truncate attribute', () => {
    context('truncate at character link', () => {
      cy.get('[data-truncate-characters]')
        .invoke('attr', 'data-truncate-characters')
        .should('equal', '20')

      cy.get('[data-truncate-characters]').should(($div) => {
        expect($div[0].textContent.length).to.eql(20)
        expect($div[0].getAttribute('title')).not.to.eql($div[0].textContent)
      })
    })

    context('truncate at px width', () => {
      cy.get('[data-truncate-px]').invoke('outerWidth').should('equal', 160)

      cy.get('[data-truncate-px]')
        .invoke('attr', 'data-truncate-px')
        .should('equal', '160')

      cy.get('[data-truncate-px]').should(($div) => {
        expect($div[0].getAttribute('title')).to.eql($div[0].textContent)
      })
    })
  })
})
