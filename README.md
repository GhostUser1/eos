# EOS Design System
![Open Source Love png2](https://badges.frapsoft.com/os/v2/open-source.png?v=103)
![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)

### Installation

1. `git clone https://gitlab.com/SUSE-UIUX/eos.git/`
2. `cd eos`
3. Make sure you have the corresponding version of Node and NPM. In order to do so,
you can change them manually or using a version manager.
The procedure for both approaches may vary depending on your OS.

    If you're using a package manager, we recommend [n](https://github.com/tj/n). This is how to set it
    up in most systems:

      a.

    ```
    npm cache clean -f
    npm install -g n
    n 10.15.0
    ```

      b.
        ```
          npm install -g npm@6.9.0
        ```

4. EOS uses Strapi CMS to manage its content. Follow one of the following:

  a - **Setup your own Strapi** and configure it to run with EOS by following our [documentation](https://gitlab.com/SUSE-UIUX/eos/-/wikis/Strapi-CMS).

  b - **Use EOS's Strapi** if you want to give EOS a try or if you are developing or contributing to it. For this option all you need to do is export the next ENV vars:

  **In your .env file:**
  ```
  EOS_STRAPI_SERVER_DEV=https://eos-strapi-dev.herokuapp.com
  EOS_STRAPI_USERNAME_DEV=testing
  EOS_STRAPI_PASSWORD_DEV=_testingStrapi_
  EOS_RUN_PROJECT_AS='dev'
  ```

5. Install EOS's dependencies with `npm install --engine-strict`. The second argument (`--engine-strict`) makes sure that you are installing dependencies using the same version of Node and NPM as the EOS.
6. Now that you have all dependencies ready, you can run the server with:

  a - `npm start` to run it normally. Make sure you have the variables set in the .env file.
  b - `npm run env:prod` if you want to run it as production (add your env variables for production or edit config.json before runing the command).
  c - `npm run env:staging` if you want to run staging.
  d - `npm run env:dev` if you want to make sure that you run development.

7. Ready! Now you can visit: http://localhost:3000/
8. If you want to test EOS DEV environment with your production Strapi or the other way around, you can use set the variable EOS_RUN_PROJECT_AS to 'dev' or 'prod'
NOTE: You have to set your production credentiales
10. If you are developing EOS and want livereload, then run `npm run browsersync` in parallel in a new terminal.

#### Working on the Backend?

You might want to redeploy Node.js everytime changes in the backend are applied:

For this you need to install Nodemon: `npm install -g nodemon`,  and instead of running `npm start` now simply run `nodemon`. It will watch your JS files and restart the server for every change.

More info: https://github.com/remy/nodemon

### Running lints:

Test all:
`npm run test:all`

Sass:
`npm run test:sass`

JS:
`npm run test:js`

Pug:
`npm run test:pug`

Unit testing:
`npm run test:unit`

End to end testing:
`npm run test:e2e`

### Building vendors

Everytime you run `npm install`, EOS will automatically build the vendors. However, you may want to run it manually if you install something manually under the vendors package. To compile the vendors files run:
`npm run build:vendors`

### Need to run more than one node versions for this and other projects?

Consider using one of the following Node version managers:

https://github.com/nodenv/nodenv

https://github.com/tj/n

# Learn more about the EOS Design System

* [EOS Design System](https://www.eosdesignsystem.com/)

* [EOS Icons](icons.eosdesignsystem.com/)

* [Follow us on Twitter](https://twitter.com/eosdesignsystem)

* [Join us in Slack](https://slack.eosdesignsystem.com/)

# Our "thank you" section

### Tested for every browser in every device

Thanks to [Browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.

### External assets
[Professional programmer - Designed by Freepik](https://www.freepik.com/free-vector/professional-programmer-engineer-writing-code_1311615.htm)
[Hipster characters - Designed by Freepik](https://www.freepik.com/free-vector/5-hipster-characters-vector-pack_760827.htm)
[Musical notes - Designed by Freepik](https://www.freepik.com/free-vector/musical-notes_795161.htm)
[Sunny day with a barbecue - Designed by Freepik](https://www.freepik.com/free-vector/sunny-day-with-a-barbecue-party-background_888237.htm)

### Projects using EOS

- [openSUSE](https://www.opensuse.org/)
- [HAWK](https://hawk-ui.github.io/)
