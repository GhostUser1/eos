const getSuccessStories = (callback) => {
  const query = `successes(where: {enabled:true}) { title, description, project_link, image_thumb, image_1, image_2, image_3, image_4}`

  $.when(
    $.ajax({
      url: `/api/strapi?q=${query}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`there was an error retrieving the data`)
      }
    })
  ).then(function (data) {
    callback(data)
  })
}
