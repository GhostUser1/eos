$(function () {
  // toggle-password
  $('.js-example-inner-box').on('click', '.js-toggle-password', function () {
    const text = $(this).text()
    $(this).text(text === 'visibility' ? 'visibility_off' : 'visibility')

    const inputType = $('.js-example-inner-box input').attr('type')
    if (inputType === 'password') {
      $('.js-example-inner-box input').attr('type', 'text')
    } else {
      $('.js-example-inner-box input').attr('type', 'password')
    }
  })
})
